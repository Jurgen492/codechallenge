package _2imagine.jurgen.codechallenge;

import java.util.Scanner;

import _2imagine.jurgen.codechallenge.calculation.Calculation;

/**
 * Created by Jurgen on 1-9-2016.
 */
public class Challenge {
    public static void main(String [ ] args)
    {
        Scanner reader = new Scanner(System.in);
        System.out.println("Input below and press enter");
        String in = reader.next();
        System.out.println(new Calculation().getResult(in));
    }
}
