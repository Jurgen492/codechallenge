package _2imagine.jurgen.codechallenge.calculation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import _2imagine.jurgen.codechallenge.operators.OperationDefinition;

/**
 * Created by Jurgen on 1-9-2016.
 */
public class Calculation {
    public String getResult(String input) {
        return evaluate(input.split(" "));
    }

    private String evaluate(String[] input) {
        boolean isNumber;
        List<String> latestInput = Arrays.asList(input);
        for (int currentOrder = OperationDefinition.getHighestOrder(); currentOrder >= 0; currentOrder--) {
            List<String> currentInput = new ArrayList<>();
            Set<OperationDefinition> operations = OperationDefinition.getByOrder(currentOrder);
            isNumber = true;
            for (int j=0; j < latestInput.size(); j++) {
                if (!isNumber) {
                    OperationDefinition currentOperation = OperationDefinition.getBySymbol(latestInput.get(j));
                    if (operations.contains(currentOperation)) {
                        System.out.print(true);
                        int tempResult = currentOperation.getOperation().doCalculate(Integer.valueOf(latestInput.get(j-1)), Integer.valueOf(latestInput.get(j+1)));
                        j++;
                        System.out.println(tempResult);
                        System.out.println(currentOperation);
                        while(operations.contains(OperationDefinition.getBySymbol(latestInput.get(j)))) {
                            currentOperation = OperationDefinition.getBySymbol(latestInput.get(j));
                            tempResult = currentOperation.getOperation().doCalculate(tempResult, Integer.valueOf(latestInput.get(j+1)));
                            j++;
                        }
                        currentInput.add(String.valueOf(tempResult));
                    } else {
                        currentInput.add(latestInput.get(j-1));
                        currentInput.add(latestInput.get(j));
                    }
                }
                if (j == latestInput.size() - 1) {
                    currentInput.add(latestInput.get(j));
                }
            }
            latestInput = new ArrayList<>(currentInput);
        }

        return latestInput.get(0);
    }
}
