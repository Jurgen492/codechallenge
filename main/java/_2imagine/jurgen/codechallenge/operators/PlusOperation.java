package _2imagine.jurgen.codechallenge.operators;

/**
 * Created by Jurgen on 1-9-2016.
 */
public class PlusOperation implements OperationIF {

    @Override
    public int doCalculate(int a, int b) {
        return a + b;
    }
}
