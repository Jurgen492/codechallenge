package _2imagine.jurgen.codechallenge.operators;

/**
 * Created by Jurgen on 1-9-2016.
 */
public interface OperationIF {

    int doCalculate(int a, int b);

}
