package _2imagine.jurgen.codechallenge.operators;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jurgen on 1-9-2016.
 */
public enum OperationDefinition {
    PLUS(0, "+", new PlusOperation()),
    MINUS(0, "-", new MinusOperation()),
    MULTIPLY(1, "*", new MultiplyOperation()),
    DIVIDE(1, "/", new DivideOperation());

    int order;
    String symbol;
    OperationIF operation;

    private static final Map<String, OperationDefinition> BY_SYMBOL = new HashMap<String, OperationDefinition>();
    private static final Map<Integer, Set<OperationDefinition>> BY_ORDER = new HashMap<Integer, Set<OperationDefinition>>();
    private static int highestOrder;
    static {
        for (OperationDefinition od : OperationDefinition.values()) {
            BY_SYMBOL.put(od.symbol, od);
            if (od.order > highestOrder) {
                highestOrder = od.order;
            }
            Set<OperationDefinition> operationDefinitionSet = BY_ORDER.get(od.order);
            if (operationDefinitionSet == null) {
                operationDefinitionSet = new HashSet<>();
                operationDefinitionSet.add(od);
                BY_ORDER.put(od.order, operationDefinitionSet);
            } else {
                operationDefinitionSet.add(od);
            }
        }
    }

    OperationDefinition(int order, String symbol, OperationIF operation) {
        this.order = order;
        this.symbol = symbol;
        this.operation = operation;
    }

    public static OperationDefinition getBySymbol(String symbol) {
        return BY_SYMBOL.get(symbol);
    }

    public static int getHighestOrder() {
        return highestOrder;
    }

    public static Set<OperationDefinition> getByOrder(int order) {
        return BY_ORDER.get(order);
    }

    public int getOrder() {
        return order;
    }

    public String getSymbol() {
        return symbol;
    }

    public OperationIF getOperation() {
        return operation;
    }
}
