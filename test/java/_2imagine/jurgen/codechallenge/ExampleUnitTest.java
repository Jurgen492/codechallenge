package _2imagine.jurgen.codechallenge;

import org.junit.Test;

import _2imagine.jurgen.codechallenge.calculation.Calculation;
import _2imagine.jurgen.codechallenge.operators.DivideOperation;
import _2imagine.jurgen.codechallenge.operators.MinusOperation;
import _2imagine.jurgen.codechallenge.operators.MultiplyOperation;
import _2imagine.jurgen.codechallenge.operators.PlusOperation;

import static org.junit.Assert.*;


public class ExampleUnitTest {
    @Test
    public void isCorrect() throws Exception {
        assertEquals("6", new Calculation().getResult("1 + 2 + 3"));
    }

    @Test
    public void operationsWork() throws Exception {
        assertEquals(6, new PlusOperation().doCalculate(4,2));
        assertEquals(2, new MinusOperation().doCalculate(4,2));
        assertEquals(-2, new MinusOperation().doCalculate(2,4));
        assertEquals(2, new DivideOperation().doCalculate(4,2));
        assertEquals(8, new MultiplyOperation().doCalculate(4,2));
    }
}